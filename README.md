# Canterlot Clips

A barbershop simulation

# Usage

This is a cli application. It doesn't take any parameters or flags. You will need golang version 1.17.5 or newer

To compile and run the simulation execute the following command:

```sh
go run ./main.go
```

## Running the tests

This project has some unit tests. To run the tests you can use the following command to run them all

```sh
go test -v ./...
```

# Implementation

I created model classes to represent the various things in this simulation. Two types of people a Barber and a Customer.
A type for the actual Barbershop. Finally, a simulation object to handle the loop and some aspects of the rules. 
There are unit tests for all the model objects as well.

## Pipeline

See the [pipeline](https://gitlab.com/gidjin/canterlot-clips/-/pipelines) which was setup using a gitlab template as an
experiment to see what working with gitlab CI/CD is like.

# Problems encountered

I typically practice Test Driven Development so I started with that. However, due to the time box I decided to stop
to allow for more time to complete some more features. Initially I wanted to simplify things and put all the logic
in the simulation class. However, it turned out that was making the simulation class too big. So my last improvement
was to add some of that logic to the Customer class.

# Improvements

Some ways I would improve this given more time

- Look into using golang's async functions to time customer cuts
- Read configuration file that specified a list of customer data 
- Add unit tests for BarbershopSimulation
- Add rest of unit tests for other classes like Customer.AddCustomer
- Fix the `composition literal uses unkeyed fields` warning

# Unimplemented Requirements

- Barbers start cutting hair of customers when they enter
- Customers leave frustrated if they wait for 20 minutes in the waiting room
- Barbers finish cutting hair
- Customer leaves satisfied
- Barbers stay if shift ends but they are cutting a customer's hair
