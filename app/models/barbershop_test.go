package models

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBarbershop(t *testing.T) {
	t.Run("Create a Barbershop", func(t *testing.T) {
		shop := NewBarbershop("Canterlot Clips")
		assert.NotNil(t, shop)
		assert.Equal(t, "Canterlot Clips", shop.Name)
		assert.Equal(t, 9, shop.Opens.Hour(), "Opening Hour")
		assert.Equal(t, 0, shop.Opens.Minute(), "Opening Minute")
		assert.Equal(t, 17, shop.Closes.Hour(), "Closing Hour")
		assert.Equal(t, 0, shop.Closes.Minute(), "Closing Minute")
	})

	t.Run("Has list of Barbers", func(t *testing.T) {
		shop := NewBarbershop("Canterlot Clips")
		shop.Barbers = append(shop.Barbers, NewBarber("Rainbow Dash", Morning))
		assert.Equal(t, 1, len(shop.Barbers))
		assert.Equal(t, "Rainbow Dash", shop.Barbers[0].Name)
	})

	t.Run("Has list of Customers", func(t *testing.T) {
		shop := NewBarbershop("Canterlot Clips")
		shop.Customers = append(shop.Customers, NewCustomer("Rainbow Dash"))
		assert.Equal(t, 1, len(shop.Customers))
		assert.Equal(t, "Rainbow Dash", shop.Customers[0].Name)
	})
}
