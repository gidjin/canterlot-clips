package models

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBarber(t *testing.T) {
	t.Run("Create a Barber", func(t *testing.T) {
		barber := NewBarber("Fluttershy", Morning)
		assert.NotNil(t, barber)
		assert.Equal(t, "Fluttershy", barber.Name)
		assert.Equal(t, Morning, barber.Shift)
	})

	t.Run("Create afternoon shift Barber", func(t *testing.T) {
		barber := NewBarber("Fluttershy", Afternoon)
		assert.Equal(t, Afternoon, barber.Shift)
	})

	t.Run("Barber can cut", func(t *testing.T) {
		barber := NewBarber("Fluttershy", Afternoon)
		assert.Equal(t, false, barber.IsCutting)
		barber.IsCutting = true
		assert.Equal(t, true, barber.IsCutting)
	})
}
