package models

type Shift string

const (
	Morning   Shift = "morning"
	Afternoon Shift = "afternoon"
)

type Barber struct {
	Name      string
	Shift     Shift
	IsCutting bool
}

func NewBarber(name string, shift Shift) Barber {
	return Barber{Name: name, Shift: shift}
}
