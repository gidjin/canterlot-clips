package models

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestEvent(t *testing.T) {
	t.Run("Create an Event", func(t *testing.T) {
		expectedTime := time.Date(2020, 11, 12, 9, 7, 0, 0, time.UTC)
		expectedEvent := "[09:07] something has happened"
		event := NewEvent(expectedTime, "something has happened")
		assert.NotNil(t, event)
		assert.Equal(t, expectedEvent, event.String())
		assert.Equal(t, expectedEvent, fmt.Sprintf("%v", event))
	})
}
