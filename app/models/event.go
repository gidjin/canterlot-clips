package models

import (
	"fmt"
	"time"
)

type Event struct {
	Time   time.Time
	Action string
}

func NewEvent(time time.Time, action string) Event {
	return Event{
		Time:   time,
		Action: action,
	}
}

func (e Event) String() string {
	return fmt.Sprintf("[%s] %s", e.Time.Format("15:04"), e.Action)
}
