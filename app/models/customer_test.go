package models

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCustomer(t *testing.T) {
	t.Run("Create a Customer", func(t *testing.T) {
		customer := NewCustomer("Fluttershy")
		assert.NotNil(t, customer)
		assert.Equal(t, "Fluttershy", customer.Name)
	})

	feelings := []Feeling{
		Unfulfilled,
		Cursing,
		Satisfied,
		Frustrated,
		Disappointed,
	}

	for _, feeling := range feelings {
		t.Run(fmt.Sprintf("Customer can feel %s", feeling), func(t *testing.T) {
			customer := NewCustomer("Fluttershy")
			assert.Nil(t, customer.Feels)
			customer.Feels = &feeling
			assert.Equal(t, feeling, *customer.Feels)
		})
	}

	t.Run("Returns a random time for completing haircut in Minutes between 20 and 40 minutes", func(t *testing.T) {
		customer := NewCustomer("Fluttershy")
		assert.GreaterOrEqual(t, customer.TimeToCompleteCut, 20)
		assert.LessOrEqual(t, customer.TimeToCompleteCut, 40)
	})
}
