package models

import (
	"fmt"
	"time"
)

type Barbershop struct {
	Name      string
	Opens     time.Time
	Closes    time.Time
	Barbers   []Barber
	Customers []Customer
}

func NewBarbershop(name string) *Barbershop {
	opens := time.Date(2020, 11, 12, 9, 0, 0, 0, time.UTC)
	closes := time.Date(2020, 11, 12, 17, 0, 0, 0, time.UTC)
	return &Barbershop{Name: name, Opens: opens, Closes: closes}
}

func (b *Barbershop) AddCustomer(customer Customer) error {
	if len(b.Customers) == 8 {
		return fmt.Errorf("Too many customers waiting")
	}
	b.Customers = append(b.Customers, customer)
	return nil
}
