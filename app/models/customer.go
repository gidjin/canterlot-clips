package models

import (
	"math/rand"
	"time"
)

type Feeling string

const (
	Unfulfilled  Feeling = "unfulfilled"
	Cursing      Feeling = "cursing"
	Satisfied    Feeling = "satisfied"
	Frustrated   Feeling = "frustrated"
	Disappointed Feeling = "disappointed"
)

type Customer struct {
	Name              string
	Feels             *Feeling
	TimeToCompleteCut int
}

func NewCustomer(name string) Customer {
	rand.Seed(time.Now().UnixNano())
	return Customer{
		Name:              name,
		Feels:             nil,
		TimeToCompleteCut: rand.Intn(20) + 20,
	}
}
