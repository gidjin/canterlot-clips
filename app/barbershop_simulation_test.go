package app

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBarbershopSimulation(t *testing.T) {
	t.Run("Create a Barbershop", func(t *testing.T) {
		sim := NewBarbershopSimulation()
		assert.NotNil(t, sim)
		events := sim.RunSimulation()
		assert.GreaterOrEqual(t, len(events), 1, "Expect to have some events")
	})

	t.Run("Barbershop should open", func(t *testing.T) {
		sim := NewBarbershopSimulation()
		assert.NotNil(t, sim)
		events := sim.RunSimulation()
		assert.Equal(t, "[09:00] Canterlot Clips is open for business!", events[0].String())
	})

	t.Run("Barbershop should close", func(t *testing.T) {
		sim := NewBarbershopSimulation()
		assert.NotNil(t, sim)
		events := sim.RunSimulation()
		assert.Equal(t, "[17:00] Canterlot Clips is closed", events[len(events)-1].String())
	})
}
