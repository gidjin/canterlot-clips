package app

import (
	"fmt"
	"gitlab.com/gidjin/canterlot-clips/app/models"
	"time"
)

type BarbershopSimulation struct {
	barbershop *models.Barbershop
}

func NewBarbershopSimulation() BarbershopSimulation {
	return BarbershopSimulation{
		barbershop: models.NewBarbershop("Canterlot Clips"),
	}
}

func barberStartsShift(currentTime time.Time, barber models.Barber) models.Event {
	return models.NewEvent(currentTime, fmt.Sprintf("%s started shift", barber.Name))
}

func barberEndsShift(currentTime time.Time, barber models.Barber) models.Event {
	return models.NewEvent(currentTime, fmt.Sprintf("%s ended shift", barber.Name))
}

func (b BarbershopSimulation) RunSimulation() []models.Event {
	var events []models.Event
	currentTime := b.barbershop.Opens

	barbers := []models.Barber{
		models.NewBarber("Spike", models.Afternoon),
		models.NewBarber("Pinkie Pie", models.Afternoon),
		models.NewBarber("Scootaloo", models.Afternoon),
		models.NewBarber("Twilight Sparkle", models.Afternoon),
	}

	// shop opens
	events = append(events, models.Event{currentTime, fmt.Sprintf("%s is open for business!", b.barbershop.Name)})
	morningShiftWorkers := []models.Barber{
		models.NewBarber("Fluttershy", models.Morning),
		models.NewBarber("Rainbow Dash", models.Morning),
		models.NewBarber("Rarity", models.Morning),
		models.NewBarber("Apple Jack", models.Morning),
	}
	for _, barber := range morningShiftWorkers {
		b.barbershop.Barbers = append(b.barbershop.Barbers, barber)
		events = append(events, barberStartsShift(currentTime, barber))
	}

	shiftChange := currentTime.Add(4 * time.Hour)
	customerId := 0

	for currentTime.Before(b.barbershop.Closes) {

		// if we are at shift change let them leave
		if currentTime.After(shiftChange) && len(b.barbershop.Barbers) > 0 {
			if b.barbershop.Barbers[0].Shift == models.Morning {
				var barber models.Barber
				barber, b.barbershop.Barbers = b.barbershop.Barbers[0], b.barbershop.Barbers[1:]
				events = append(events, barberEndsShift(currentTime, barber))
			}
		}

		if currentTime.After(shiftChange) && len(b.barbershop.Barbers) < 4 && len(barbers) > 0 {
			var barber models.Barber
			barber, barbers = barbers[0], barbers[1:]
			b.barbershop.Barbers = append(b.barbershop.Barbers, barber)
			events = append(events, barberStartsShift(currentTime, barber))
		}

		diff := currentTime.Sub(b.barbershop.Opens)
		// a customer shows up every 5 minutes
		if diff%(5*time.Minute) == 0 {
			customer := models.NewCustomer(fmt.Sprintf("Customer %d", customerId))
			customerId = customerId + 1
			err := b.barbershop.AddCustomer(customer)
			if err != nil {
				events = append(events, models.Event{currentTime, fmt.Sprintf("%s leaves unfulfilled", customer.Name)})
			} else {
				events = append(events, models.Event{currentTime, fmt.Sprintf("%s entered", customer.Name)})
			}
		}

		currentTime = currentTime.Add(1 * time.Minute)
	}

	for _, barber := range b.barbershop.Barbers {
		events = append(events, barberEndsShift(currentTime, barber))
	}

	for _, customer := range b.barbershop.Customers {
		events = append(events, models.Event{currentTime, fmt.Sprintf("%s leaves cursing", customer.Name)})
	}

	// shop is closed
	events = append(events, models.Event{currentTime, fmt.Sprintf("%s is closed", b.barbershop.Name)})

	return events
}
