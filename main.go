package main

import (
	"fmt"
	"gitlab.com/gidjin/canterlot-clips/app"
)

// main runs the command-line parsing and validations. This function will also start the application logic execution.
func main() {
	// Run the App
	barberShopSim := app.NewBarbershopSimulation()

	events := barberShopSim.RunSimulation()

	for _, event := range events {
		fmt.Printf("%v\n", event)
	}
}
